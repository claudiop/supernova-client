use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};

use crate::config::{APP_ID, PROFILE};


use gtk::CompositeTemplate;

#[derive(Debug, CompositeTemplate)]
#[template(resource = "/pt/nunl/supernova/ui/window.ui")]
pub struct MainApplicationWindow {
    #[template_child]
    pub headerbar: TemplateChild<gtk::HeaderBar>,
    #[template_child]
    pub root_stack: TemplateChild<adw::ViewStack>,
    pub settings: gio::Settings,
}

impl Default for MainApplicationWindow {
    fn default() -> Self {
        Self {
            headerbar: TemplateChild::default(),
            root_stack: TemplateChild::default(),
            settings: gio::Settings::new(APP_ID),
        }
    }
}

#[glib::object_subclass]
impl ObjectSubclass for MainApplicationWindow {
    const NAME: &'static str = "MainApplicationWindow";
    type Type = super::MainApplicationWindow;
    type ParentType = gtk::ApplicationWindow;

    fn class_init(klass: &mut Self::Class) {
        Self::bind_template(klass);
    }

    // You must call `Widget`'s `init_template()` within `instance_init()`.
    fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for MainApplicationWindow {
    fn constructed(&self, obj: &Self::Type) {
        self.parent_constructed(obj);

        // Devel Profile
        if PROFILE == "Devel" {
            obj.add_css_class("devel");
        }

        // Load latest window state
        obj.load_window_size();

        //self.root_stack.node
        //let blur = gsk::BlurNode::new(&self.root_stack, 10.0);
        //self.root_stack.blur
    }
}

impl WidgetImpl for MainApplicationWindow {}
impl WindowImpl for MainApplicationWindow {
    // Save window state on delete event
    fn close_request(&self, window: &Self::Type) -> gtk::Inhibit {
        if let Err(err) = window.save_window_size() {
            log::warn!("Failed to save window state, {}", &err);
        }

        // Pass close request on to the parent
        self.parent_close_request(window)
    }
}

impl ApplicationWindowImpl for MainApplicationWindow {}

