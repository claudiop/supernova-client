use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

#[derive(Debug, CompositeTemplate)]
#[template(file = "/pt/nunl/supernova/ui/auth.ui")]
pub struct AuthDialog {
    // Complete
}

impl Default for AuthDialog {
    fn default() -> Self {
        Self {}
    }
}

#[glib::object_subclass]
impl ObjectSubclass for AuthDialog {
    const NAME: &'static str = "AuthDialog";
    type Type = super::AuthDialog;
    type ParentType = gtk::Dialog;

    fn class_init(klass: &mut Self::Class) {
        Self::bind_template(klass);
    }

    fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for AuthDialog {
    fn constructed(&self, obj: &Self::Type) {
        self.parent_constructed(obj);
    }
}

impl WidgetImpl for AuthDialog {}
impl WindowImpl for AuthDialog {}
impl DialogImpl for AuthDialog {}
