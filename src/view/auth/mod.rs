mod imp;

use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib, gsk};

use crate::view::application::MainApplication;

glib::wrapper! {
    pub struct AuthDialog(ObjectSubclass<imp::AuthDialog>)
        @extends gtk::Widget, gtk::Window, gtk::Dialog,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl AuthDialog {
    pub fn new(app: &MainApplication) -> Self {
        glib::Object::new(&[("application", app)]).expect("Failed to create AuthDialog")
    }
}
