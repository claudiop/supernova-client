use super::*;
use glib::WeakRef;
use once_cell::sync::OnceCell;

#[derive(Debug, Default)]
pub struct MainApplication {
    pub window: OnceCell<WeakRef<MainApplicationWindow>>,
}

#[glib::object_subclass]
impl ObjectSubclass for MainApplication {
    const NAME: &'static str = "MainApplication";
    type Type = super::MainApplication;
    type ParentType = gtk::Application;
}

impl ObjectImpl for MainApplication {}

impl ApplicationImpl for MainApplication {
    fn activate(&self, app: &Self::Type) {
        debug!("GtkApplication<MainApplication>::activate");

        if let Some(window) = self.window.get() {
            let window = window.upgrade().unwrap();
            window.show();
            window.present();
            return;
        }

        let window = MainApplicationWindow::new(app);
        self.window
            .set(window.downgrade())
            .expect("Window already set.");

        app.main_window().present();
    }

    fn startup(&self, app: &Self::Type) {
        debug!("GtkApplication<MainApplication>::startup");
        self.parent_startup(app);

        // Set icons for shell
        gtk::Window::set_default_icon_name(APP_ID);

        app.setup_css();
        app.setup_gactions();
        app.setup_accels();
    }
}

impl GtkApplicationImpl for MainApplication {}